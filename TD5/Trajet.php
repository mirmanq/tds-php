<?php

require_once __DIR__ . '/src/Modele/ConnexionBaseDeDonnees.php';
require_once __DIR__ . '/src/Modele/Utilisateur.php';

class Trajet {

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private ModeleUtilisateur $conducteurLogin;
    private bool $nonFumeur;
    private array $passagers;

    public function __construct(
        ?int              $id,
        string            $depart,
        string            $arrivee,
        DateTime          $date,
        int               $prix,
        ModeleUtilisateur $conducteurLogin,
        bool              $nonFumeur,
        array             $passagers = []
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteurLogin = $conducteurLogin;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = $passagers;
    }

    public static function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
    $trajet = new Trajet(
        $trajetTableau["id"],
        $trajetTableau["depart"],
        $trajetTableau["arrivee"],
        new DateTime($trajetTableau["date"]),
        $trajetTableau["prix"],
        ModeleUtilisateur::recupererUtilisateurParLogin($trajetTableau["conducteurLogin"]),
        $trajetTableau["nonFumeur"],
        []
    );

    // Retrieve and set the passengers
    $trajet->passagers = $trajet->recupererPassagers();

    return $trajet;
}

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteurLogin(): ModeleUtilisateur
    {
        return $this->conducteurLogin;
    }

    public function setConducteur(ModeleUtilisateur $conducteurLogin): void
    {
        $this->conducteurLogin = $conducteurLogin;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function getPassagers(): array
    {
        return $this->passagers;
    }

    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }

    public function __toString()
{
    $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
    $passagersStr = "";

    foreach ($this->passagers as $passager) {
        $passagersStr .= $passager->getPrenom() . " Trajet.php" . $passager->getNom() . " (" . $passager->getLogin() . "), ";
    }

    // Remove the trailing comma and space
    $passagersStr = rtrim($passagersStr, ", ");

    return "<p>
        Le trajet$nonFumeur du {$this->date->format("d/m/Y")} 
        partira de {$this->depart} pour aller à {$this->arrivee} 
        (conducteurLogin: {$this->conducteurLogin->getPrenom()} 
        {$this->conducteurLogin->getNom()} {$this->conducteurLogin->getLogin()}) pour {$this->prix}€. 
        Il y a les passagers suivants: $passagersStr.
    </p>";
}

    /**
     * @return Trajet[]
     */
    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    public function ajouter() : void {
        $pdo = ConnexionBaseDeDonnees::getPDO();
        $pdoStatement = $pdo->prepare("INSERT INTO trajet (depart, arrivee, date, prix, conducteurLogin, nonFumeur) VALUES (:depart, :arrivee, :date, :prix, :conducteurLogin, :nonFumeur)");

        $f = 0;
       if($this->nonFumeur){
            $f = 1;
        }
       $pdoStatement->execute([
            "depart" => $this->depart,
            "arrivee" => $this->arrivee,
            "date" => $this->date->format("Y-m-d"),
            "prix" => $this->prix,
            "conducteurLogin" => $this->conducteurLogin->getLogin(),
            "nonFumeur" => $f
        ]);
    }

    private function recupererPassagers() : array {
    $pdo = ConnexionBaseDeDonnees::getPDO();
    $pdoStatement = $pdo->prepare("SELECT * FROM passager p JOIN utilisateur u ON p.passagerLogin = u.login WHERE p.trajetId = :trajetId;");
    $pdoStatement->execute([
        "trajetId" => $this->id // Bind the trajetId parameter
    ]);

    $passagers = [];
    foreach($pdoStatement as $passagerFormatTableau) {
        $passagers[] = ModeleUtilisateur::construireDepuisTableauSQL($passagerFormatTableau); // Add passengers to an array
    }

    return $passagers; // Return the array
}
}
