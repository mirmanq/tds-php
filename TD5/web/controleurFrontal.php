<?php
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

use App\Covoiturage\Controleur\ControleurUtilisateur;

// initialisation en activant l'affichage de débogage
//$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(true);
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

$controleur = 'Utilisateur';
if (isset($_GET['controleur'])) {
    $controleur = ucfirst($_GET['controleur']);
}
$nomDeClasseControleur = 'App\Covoiturage\Controleur\Controleur' . $controleur;
if (class_exists($nomDeClasseControleur)) {
    if (isset($_GET['action'])) {
        $liste_actions = get_class_methods($nomDeClasseControleur); // liste des méthodes de la classe ControleurUtilisateur

        if (in_array($_GET['action'], $liste_actions)) {
            $action = $_GET['action'];
            $nomDeClasseControleur::$action();
        }
        else {
            ControleurUtilisateur::afficherErreur('Action inconnue');
        }
    }
    else {
        $action = 'afficherListe';
        ControleurUtilisateur::$action();
    }
}
else {
    ControleurUtilisateur::afficherErreur('Classe inconnue');
}

?>