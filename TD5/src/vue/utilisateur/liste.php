<!DOCTYPE html>
<html>
<body>
<h1>Liste des utilisateurs</h1>
<!-- Ajoutez le lien ici -->
<a href="/tds-php/TD5/web/controleurFrontal.php?action=afficherFormulaireCreation">Créer un utilisateur</a>
<a href="/tds-php/TD5/web/controleurFrontal.php?action=supprimer">supprimer un utilisateur</a>
<ul>
    <?php
    /** @var Utilisateur[] $utilisateurs */
    foreach ($utilisateurs as $utilisateur):
        $loginHTML = htmlspecialchars($utilisateur->getLogin()); // celui ci sera affiché
        $loginURL = rawurlencode($utilisateur->getLogin()); // celui ci sera dans l'url
        ?>
        <li>
            <a href="/tds-php/TD5/web/controleurFrontal.php?action=afficherDetail&login=<?php echo $loginURL; ?>">
                <?php echo $loginHTML; ?>
            </a>
            <?php echo "     -     " ?>
            <a href="/tds-php/TD5/web/controleurFrontal.php?action=afficherFormulaireMiseAJour&login=<?php echo $loginURL; ?>">  "modifier" </a>

    <?php endforeach; ?>
</ul>
</body>
</html>