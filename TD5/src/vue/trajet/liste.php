<!DOCTYPE html>
<html>
<body>
<h1>Liste des trajets</h1>
<!-- Ajoutez le lien ici -->
<!-- <a href="/PHP/TD5/web/controleurFrontal.php?action=afficherFormulaireCreation">Créer un utilisateur</a>
<a href="/PHP/TD5/web/controleurFrontal.php?action=supprimer">supprimer un utilisateur</a> -->
<ul>
    <?php
    /** @var \App\Covoiturage\Modele\DataObject\Trajet[] $trajets */
    foreach ($trajets as $trajet):
    $idHTML = htmlspecialchars($trajet->getId()); // celui ci sera affiché
    $idURL = rawurlencode($trajet->getId()); // celui ci sera dans l'url
    ?>
    <li>
        <a>
            <?php echo
            $trajet->__toString()
            ; ?>
        </a>
    </li>
    <?php endforeach; ?>
</ul>
</body>
</html>