<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="/tds-php/TD5/ressources/style.css">
        <meta charset="UTF-8">
        <title><?php
                /**
                 * @var string $titre
                 */
                echo $titre; ?>
        </title>
    </head>
    <body>
        <header>
            <nav>
                <nav>
                    <ul>
                        <li>
                            <a href="/tds-php/TD5/web/controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
                        </li><li>
                            <a href="/tds-php/TD5/web/controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
                        </li>
                    </ul>
                </nav>
            </nav>
        </header>
        <main>
            <?php
            /**
             * @var string $cheminCorpsVue
             */
            require __DIR__ . "/{$cheminCorpsVue}";
            ?>
        </main>
    <footer>
        <p>
            Site de covoiturage de Quentin Company - 2020
        </p>
    </footer>
    </body>
</html>
