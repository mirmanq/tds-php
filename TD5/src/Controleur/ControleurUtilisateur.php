<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur {

    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer();

        $parametres = [
            'titre' => 'Liste des utilisateurs',
            'cheminCorpsVue' => 'utilisateur/liste.php',
            'utilisateurs' => $utilisateurs
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function afficherDetail() : void {
        $login = $_GET['login'];
        $utilisateur = UtilisateurRepository::recupererUtilisateurParLogin($login);

        $parametres = [
            'titre' => 'Détail de l\'utilisateur',
            'cheminCorpsVue' => 'utilisateur/detail.php',
            'utilisateur' => $utilisateur
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function afficherFormulaireCreation() : void {
        $parametres = [
            'titre' => 'Créer un utilisateur',
            'cheminCorpsVue' => 'utilisateur/formulaireCreation.php'
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function creerDepuisFormulaire() : void {
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];

        $utilisateur = new Utilisateur($login, $nom, $prenom);
        UtilisateurRepository::ajouterUtilisateur($utilisateur);

        $utilisateurs = AbstractRepository::recuperer();

        $parametres = [
            'titre' => 'Utilisateur créé',
            'cheminCorpsVue' => 'utilisateur/utilisateurCree.php',
            'utilisateurs' => $utilisateurs
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres);

        require __DIR__ . '/../vue/' . $cheminVue;
    }

    public static function afficherErreur(string $messageErreur = "") : void {
                self::afficherVue('vueGenerale.php',  ['titre' => 'Erreur', 'cheminCorpsVue' => 'utilisateur/erreur.php', 'messageErreur' => $messageErreur]);
    }

    public static function supprimer() : void {
        $login = $_GET['login'];
        UtilisateurRepository::supprimerParLogin($login);
        $utilisateurs = AbstractRepository::recuperer();

        self::afficherVue('vueGenerale.php' , ['titre' => 'Utilisateur supprimé',  'cheminCorpsVue' => 'utilisateur/utilisateurSupprime.php', 'login' => $login, 'utilisateurs' => $utilisateurs]);
    }

        public static function afficherFormulaireMiseAJour() : void {
        $login = $_GET['login'];
        $utilisateur = UtilisateurRepository::recupererUtilisateurParLogin($login);

        $parametres = [
            'titre' => 'Modifier un utilisateur',
            'cheminCorpsVue' => 'utilisateur/formulaireMiseAJour.php',
            'utilisateur' => $utilisateur
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function mettreAJour(){
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];

        $utilisateur = new Utilisateur($login, $nom, $prenom);
        (new \App\Covoiturage\Modele\Repository\UtilisateurRepository)->mettreAJour($utilisateur);

        $utilisateurs = AbstractRepository::recuperer();

        self::afficherVue('vueGenerale.php',
            ['titre' => 'Utilisateur modifié',
            'cheminCorpsVue' => 'utilisateur/utilisateurMisAJour.php',
            'login' => $login,
            'utilisateurs' => $utilisateurs
        ]);

    }
}
?>