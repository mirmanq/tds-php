<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\Repository\TrajetRepository;

class ControleurTrajet
{
    public static function action() {
        $trajets =  (new TrajetRepository())->recuperer();
        $vue = new Vue();
        $vue->afficherListe($trajets);
    }

    public static function afficherListe() : void {
        $trajets = (new TrajetRepository())->recuperer();

        $parametres = [
            'titre' => 'Liste des trajets',
            'cheminCorpsVue' => 'trajet/liste.php',
            'trajets' => $trajets
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres);

        require __DIR__ . '/../vue/' . $cheminVue;
    }

    public static function afficherErreur(string $messageErreur = "") : void {
        self::afficherVue('vueGenerale.php',  ['titre' => 'Erreur', 'cheminCorpsVue' => 'trajet/erreur.php', 'messageErreur' => $messageErreur]);
    }


}