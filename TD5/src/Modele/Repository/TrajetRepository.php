<?php

namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\DataObject\Trajet;
use DateTime;

class TrajetRepository extends AbstractRepository
{
    public function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        return new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            UtilisateurRepository::recupererUtilisateurParLogin($trajetTableau["conducteurLogin"]),
            $trajetTableau["nonFumeur"],
        );
    }

    /*
    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] =  TrajetRepository::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }
    */

    static public function recupererPassagers(Trajet $trajet): array {
        $pdo = ConnexionBaseDeDonnees::getPDO();
        $pdoStatement = $pdo->prepare("SELECT passagerLogin FROM passager WHERE trajetId = :trajetId");
        $values = array(
            "trajetId" => $trajet->getId(),
        );
        $pdoStatement->execute($values);
        foreach($pdoStatement as $passagerFormatTableau) {
            $passagers[] = UtilisateurRepository::recupererUtilisateurParLogin($passagerFormatTableau["passagerLogin"]);
        }
        return $passagers;
    }

    protected function getNomTable(): string {
        return "trajet";
    }

}