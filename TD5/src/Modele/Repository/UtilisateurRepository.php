<?php
namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use PDO;

class UtilisateurRepository extends AbstractRepository{


// Pour pouvoir convertir un objet en chaîne de caractères
//    public function __toString(): string
//    { //rajouter la liste des trajets comme passager
//        return '<p> prénom : ' . $this->prenom . ' - ' . 'Nom : ' . $this->nom . ' - ' . ' login : ' . $this->login . 'passager sur : ' . implode($this->getTrajetsCommePassager()) . '</p>';
//    }

    public static function recupererUtilisateurParLogin(string $login): Utilisateur {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        if ($utilisateurFormatTableau === false) {
            ControleurUtilisateur::afficherErreur('Utilisateur non trouvé');
        }
        return (new UtilisateurRepository())->construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur {
        return new Utilisateur(
            $utilisateurFormatTableau["login"],
            $utilisateurFormatTableau["nom"],
            $utilisateurFormatTableau["prenom"]
        );
    }


    /**
     * @return Trajet[]
     */
    public static function recupererTrajetsCommePassager(Utilisateur $utilisateur): array {
        $sql = "SELECT * FROM trajet WHERE loginPassager = :login";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute([
            'login' => $utilisateur->getLogin(),
        ]);
        $trajets = [];
        foreach ($pdoStatement as $trajetFormatTableau) {
            $trajets[] = TrajetRepository::construireDepuisTableauSQL($trajetFormatTableau);
        }
        return $trajets;
    }

    public static function ajouterUtilisateur(Utilisateur $utilisateur): void {
        $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:login, :nom, :prenom)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute([
            'login' => $utilisateur->getLogin(),
            'nom' => $utilisateur->getNom(),
            'prenom' => $utilisateur->getPrenom(),
        ]);
    }

    public static function supprimerParLogin($login): void {
        $sql = "DELETE FROM utilisateur WHERE login = :login";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute([
            'login' => $login,
        ]);
    }

    public function mettreAJour(Utilisateur $utilisateur): void {
        $sql = "UPDATE utilisateur SET nom = :nom, prenom = :prenom WHERE login = :login";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute([
            'login' => $utilisateur->getLogin(),
            'nom' => $utilisateur->getNom(),
            'prenom' => $utilisateur->getPrenom(),
        ]);
    }

    protected function getNomTable(): string {
        return "utilisateur";
    }
}