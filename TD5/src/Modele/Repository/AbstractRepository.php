<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;

abstract class AbstractRepository{


    /**
     * @return AbstractDataObject[]
     */
    public function recuperer(): array {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM " . $this->getNomTable());
        $values = [];

        foreach ($pdoStatement as $utilisateurFormatTableau) { // on parcourt chaque utilisateur
            $values[] = $this->construireDepuisTableauSQL($utilisateurFormatTableau); // On ajoute chaque utilisateur à un tableau d'values
        }
        return $values;
    }

    protected abstract function getNomTable(): string;

    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;

}