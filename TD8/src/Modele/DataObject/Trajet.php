<?php

namespace App\Covoiturage\Modele\DataObject;

use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use \DateTime as DateTime;

class Trajet extends AbstractDataObject
{

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private Utilisateur $conducteurLogin;
    private bool $nonFumeur;
    private array $passagers;

    public function __construct(
        ?int        $id,
        string      $depart,
        string      $arrivee,
        DateTime    $date,
        int         $prix,
        Utilisateur $conducteurLogin,
        bool        $nonFumeur,
        array       $passagers = []
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteurLogin = $conducteurLogin;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = $passagers;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteurLogin(): Utilisateur
    {
        return $this->conducteurLogin;
    }

    public function setConducteur(Utilisateur $conducteurLogin): void
    {
        $this->conducteurLogin = $conducteurLogin;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function getPassagers(): array
    {
        return $this->passagers;
    }

    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }

    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
        $passagersStr = "";

        foreach ($this->passagers as $passager) {
            $passagersStr .= $passager->getPrenom() . " Trajet.php" . $passager->getNom() . " (" . $passager->getLogin() . "), ";
        }

        // Remove the trailing comma and space
        $passagersStr = rtrim($passagersStr, ", ");

        return "
        Le trajet$nonFumeur du {$this->date->format("d/m/Y")} 
        partira de {$this->depart} pour aller à {$this->arrivee} 
        (conducteurLogin: {$this->conducteurLogin->getPrenom()} 
        {$this->conducteurLogin->getNom()} {$this->conducteurLogin->getLogin()}) pour {$this->prix}€. 
        Il y a les passagers suivants: $passagersStr.
    ";
    }


    public function ajouter(): void
    {
        $pdo = ConnexionBaseDeDonnees::getPDO();
        $pdoStatement = $pdo->prepare("INSERT INTO trajet (depart, arrivee, date, prix, conducteurLogin, nonFumeur) VALUES (:depart, :arrivee, :date, :prix, :conducteurLogin, :nonFumeur)");

        $f = 0;
        if ($this->nonFumeur) {
            $f = 1;
        }
        $pdoStatement->execute([
            "depart" => $this->depart,
            "arrivee" => $this->arrivee,
            "date" => $this->date->format("Y-m-d"),
            "prix" => $this->prix,
            "conducteurLogin" => $this->conducteurLogin->getLogin(),
            "nonFumeur" => $f
        ]);
    }

}
