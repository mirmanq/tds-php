<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use http\Cookie;

class ControleurUtilisateur extends ControleurGenerique
{

    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer();

        $parametres = [
            'titre' => 'Liste des utilisateurs',
            'cheminCorpsVue' => 'utilisateur/liste.php',
            'utilisateurs' => $utilisateurs
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function afficherDetail(): void
    {
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if ($utilisateur === null) {
            self::afficherErreur("Utilisateur non trouvé");
            return;
        }

        $parametres = [
            'titre' => 'Détail de l\'utilisateur',
            'cheminCorpsVue' => 'utilisateur/detail.php',
            'utilisateur' => $utilisateur
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function demmarerSession(): void
    {
        session_start();
        echo "Session démarrée.";
    }
    public static function afficherFormulaireCreation(): void
    {
        $parametres = [
            'titre' => 'Créer un utilisateur',
            'cheminCorpsVue' => 'utilisateur/formulaireCreation.php'
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function creerDepuisFormulaire(): void
    {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->ajouter($utilisateur);

        $utilisateurs = (new UtilisateurRepository())->recuperer();

        $parametres = [
            'titre' => 'Utilisateur créé',
            'cheminCorpsVue' => 'utilisateur/utilisateurCree.php',
            'utilisateurs' => $utilisateurs
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $login = $tableauDonneesFormulaire['login'];
        $nom = $tableauDonneesFormulaire['nom'];
        $prenom = $tableauDonneesFormulaire['prenom'];
        $mdp = $tableauDonneesFormulaire['mdp'];
        $mdp2 = $tableauDonneesFormulaire['mdp2'];
        // Ajoutez d'autres champs nécessaires ici
        if ($mdp !== $mdp2) {
            self::afficherErreur("Les mots de passe ne correspondent pas");
            exit();
        }

        //On hache le mot de passe
        $mdpHache = MotDePasse::hacher($mdp);

        //On retourne un nouvel utilisateur avec le mot de passe haché
        return new Utilisateur($login, $nom, $prenom,null, $mdpHache);
    }

//    public static function deposerCookie(): void
//    {
//        $nomCookie = 'monCookie';
//        $valeurCookie = 'valeurDuCookie';
//        $duree = time() + 3600; // 1 heure
//
//        Cookie::enregistrer($nomCookie, $valeurCookie, $duree);
//
//        echo "Cookie déposé avec succès.";
//    }
//
//    public static function lireCookie(): void
//    {
//        $nomCookie = 'monCookie';
//
//        if (isset($_COOKIE[$nomCookie])) {
//            echo "Valeur du cookie '{$nomCookie}' : {Cookie::lire($nomCookie)}";
//        } else {
//            echo "Cookie '{$nomCookie}' non trouvé.";
//        }
//    }

    public static function afficherFormulaireMiseAJour(): void
    {
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if ($utilisateur === null) {
            self::afficherErreur("Utilisateur non trouvé");
            return;
        }

        $parametres = [
            'titre' => 'Mettre à jour un utilisateur',
            'cheminCorpsVue' => 'utilisateur/formulaireMiseAJour.php',
            'utilisateur' => $utilisateur
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function mettreAJour(): void
    {
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $AncienMdp = $_GET['mdp'];
        $NouveauMdp = $_GET['mdp2'];
        $VerifMdp = $_GET['mdp3'];

        if (empty($login) || empty($nom) || empty($prenom) || empty($AncienMdp)) {
            self::afficherErreur("Tous les champs sont obligatoires");
            return;
        }


        if ($AncienMdp = null){
            $utilisateur = new Utilisateur($login, $nom, $prenom, null, $AncienMdp );
        }
        else {
            $utilisateur = new Utilisateur($login, $nom, $prenom, null, $NouveauMdp);
        }
        (new UtilisateurRepository())->mettreAJour($utilisateur);

        $utilisateurs = (new UtilisateurRepository())->recuperer();

        $parametres = [
            'titre' => 'Utilisateur mis à jour',
            'cheminCorpsVue' => 'utilisateur/utilisateurMiseAJour.php',
            'utilisateurs' => $utilisateurs
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function supprimer(): void
    {
        $login = $_GET['login'];

        if (empty($login)) {
            self::afficherErreur("Le login n'a pas été trouvé");
            return;
        }

        (new UtilisateurRepository())->supprimer($login);

        $utilisateurs = (new UtilisateurRepository())->recuperer();

        $parametres = [
            'titre' => 'Utilisateur supprimé',
            'cheminCorpsVue' => 'utilisateur/utilisateurSupprime.php',
            'utilisateurs' => $utilisateurs
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function afficherErreur(string $messageErreur = ""): void
    {
        $parametres = [
            'titre' => 'Erreur',
            'cheminCorpsVue' => 'utilisateur/erreur.php',
            'messageErreur' => $messageErreur
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function connecter(): void
    {
        $login = $_GET['login'];
        $mdp = $_GET['mdp'];

        if ($login === null OR $mdp === null) {
            self::afficherErreur("champ utilisateur ou mot de passe vide");
            return;
        }

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if ($utilisateur === null) {
            self::afficherErreur("Utilisateur non trouvé");
            return;
        }

        if (!MotDePasse::verifier($mdp, $utilisateur->getMdpHache())) {
            self::afficherErreur("Mot de passe incorrect");
            return;
        }

        ConnexionUtilisateur::connecter($login);

        $parametres = [
            'titre' => 'Connexion réussie',
            'cheminCorpsVue' => 'utilisateur/utilisateurConnecte.php',
            'utilisateur' => $utilisateur
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function afficherFormulaireConnexion(): void
    {
        $parametres = [
            'titre' => 'Connexion utilisateur',
            'cheminCorpsVue' => 'utilisateur/formulaireConnexion.php'
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function deconnecter(): void
    {
        ConnexionUtilisateur::deconnecter();

        $parametres = [
            'titre' => 'Déconnexion',
            'cheminCorpsVue' => 'utilisateur/utilisateurDeconnecte.php'
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

}

?>