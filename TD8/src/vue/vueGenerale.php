<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/tds-php/TD8/ressources/style.css">

    <title><?php
        /**
         * @var string $titre
         */

        use App\Covoiturage\Lib\ConnexionUtilisateur;

        echo $titre; ?>
    </title>
</head>
<body>
<header>
    <nav>
        <nav>
            <ul>
                <li>
                    <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des
                        utilisateurs</a>
                </li>
                <li>
                    <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des
                        trajets</a>
                </li>
                <li>
                    <a href="controleurFrontal.php?action=afficherFormulairePreference"><img src="/tds-php/TD8/ressources/img/img.png"> </a></li>
                </li>
                <?php if (ConnexionUtilisateur::estConnecte() === false): ?>
                    <li>
                        <a href="controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur"><img src="/tds-php/TD8/ressources/img/login.png"> </a></li>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur"><img src="/tds-php/TD8/ressources/img/seConnecter.png"> </a></li>
                    </li>
                <?php endif; ?>
                <?php if (ConnexionUtilisateur::estConnecte()): ?>
                    <li>
                        <a href="controleurFrontal.php?action=deconnecter&controleur=utilisateur"><img src="/tds-php/TD8/ressources/img/deconnecter.png"> </a></li>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?action=afficherDetail&controleur=utilisateur&login=<?php echo ConnexionUtilisateur::getLoginUtilisateurConnecte(); ?>"><img src="/tds-php/TD8/ressources/img/profil.png"> </a></li>
                    </li>
                <?php endif; ?>
            </ul>
        </nav>
    </nav>
</header>
<main>
    <?php
    /**
     * @var string $cheminCorpsVue
     */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de Quentin Company - 2020
    </p>
</footer>
</body>
</html>