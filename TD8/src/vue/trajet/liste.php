<h1>Liste des trajets</h1>

<a href="controleurFrontal.php?action=afficherFormulaireCreation&controleur=trajet">Créer un nouveau trajet</a>

<?php
/** @var Trajet[] $trajets */
foreach ($trajets as $trajet): ?>
    <ul>
        <td>Trajet <?= $trajet->getId() ?> :</td>
        <td><?= $trajet->getDepart() ?></td>
        <td><?= $trajet->getArrivee() ?></td>
        <td><?= $trajet->getDate()->format('d/m/Y') ?></td>
        <td><?= $trajet->getPrix() ?>€</td>
        <td>
            <a href="controleurFrontal.php?controleur=trajet&action=afficherDetail&id=<?= $trajet->getId() ?>">Détails</a>
            <a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireMiseAJour&id=<?= $trajet->getId() ?>">Modifier</a>
            <a href="controleurFrontal.php?controleur=trajet&action=supprimer&id=<?= $trajet->getId() ?>">Supprimer</a>
        </td>
    </ul>
<?php endforeach; ?>