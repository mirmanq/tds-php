<h1>Liste des utilisateurs</h1>
<a href="controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur">Créer un utilisateur</a>

<ul>
    <?php
    /** @var ModeleUtilisateur[] $utilisateurs */
    foreach ($utilisateurs as $utilisateur):
        $loginHTML = htmlspecialchars($utilisateur->getLogin()); // celui ci sera affiché
        $loginURL = rawurlencode($utilisateur->getLogin()); // celui ci sera dans l'url
        ?>
        <li>
            <a href="controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=<?php echo $loginURL; ?>">
                <strong style="font-size: larger;"><?php echo $loginHTML; ?></strong>
            </a>
<!--            <a href="controleurFrontal.php?controleur=utilisateur&action=supprimer&login=--><?php //echo $loginURL; ?><!--"> Supprimer</a>-->
<!--            <a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=--><?php //echo $loginURL; ?><!--"> Modifier</a>-->
        </li>
    <?php endforeach; ?>
</ul>