<?php
/** @var ModeleUtilisateur $utilisateur */
$loginHTML = htmlspecialchars($utilisateur->getLogin()); // celui ci sera affiché
$loginURL = rawurlencode($utilisateur->getLogin());
?>

<form method="get" action="controleurFrontal.php">
    <input type="hidden" name="action" value="mettreAJour" readonly="readonly" />
    <input type="hidden" name="controleur" value="utilisateur" />
    <fieldset>
        <legend>Formulaire de modification utilisateur :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_URL">Login&#42;</label>
            <input class="InputAddOn-field" type="text" name="login" id="login_URL" value="<?= $loginHTML; ?>" readonly="readonly" />
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom&#42;</label>
            <input class="InputAddOn-field" type="text" name="nom" id="nom_id" value="<?= htmlspecialchars($utilisateur->getNom()); ?>" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prénom&#42;</label>
            <input class="InputAddOn-field" type="text" name="prenom" id="prenom_id" value="<?= htmlspecialchars($utilisateur->getPrenom()); ?>" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Ancien mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp2_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Nouveau mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Vérification du mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp3" id="mdp2_id" required>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>