<?php
/** @var \App\Covoiturage\Modele\DataObject\Utilisateur $utilisateur */
$prenom = htmlspecialchars($utilisateur->getPrenom());
$nom = htmlspecialchars($utilisateur->getNom());
$login = htmlspecialchars($utilisateur->getLogin());
$trajets = htmlspecialchars(implode(', ', $utilisateur->getTrajetsCommePassager()));
?>

<p>Prenom : <?php echo $prenom; ?> - Nom : <?php echo $nom; ?> - Login : <?php echo $login; ?> - Trajets : <?php echo $trajets; ?></p>