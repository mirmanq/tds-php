<?php
require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle

class ControleurUtilisateur {

    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('utilisateur/liste.php', ['utilisateurs' => $utilisateurs]); //appel à la vue
    }

    public static function afficherDetail() : void {
        $id = $_GET['login'];

        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($id);
        self::afficherVue('utilisateur/detail.php', ['utilisateur' => $utilisateur]);

        //récupération des données
        $prenom = $utilisateur->getPrenom();
        $nom = $utilisateur->getNom();
        $login = $utilisateur->getLogin();
        $trajets = implode($utilisateur->getTrajetsCommePassager());

        //tout afficher
        echo "<h1>Prenom : $prenom - nom : $nom - login : ($login) - trajets : $trajets</h1>";
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation() : void {
        self::afficherVue('../vue/utilisateur/formulaireCreation.php');
    }

    public static function creerDepuisFormulaire() : void {
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];

        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);

        $utilisateur->ajouter();

        self::afficherListe();
    }
}
?>