<?php

require_once (__DIR__.'/ConnexionBaseDeDonnees.php');
class ModeleUtilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom() {
        return $this->nom;
    }

    // un setter
    public function setNom($nom) {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom
   ) {
        $this->login = substr($login,0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    public function getLogin() : string {
        return $this->login;
    }

    public function setLogin(string $login) {
        $this->login = substr($login,0, 64);;
    }

    public  function getPrenom() :string {
        return $this->prenom;
    }

    public function setPrenom(string $prenom) {
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
//    public function __toString() : string {
//        return "le nom " . $this->nom . " et le prenom " . $this->prenom . " et le login " . $this->login;
//    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : ModeleUtilisateur {
        return new ModeleUtilisateur($utilisateurFormatTableau['login'], $utilisateurFormatTableau['nom'], $utilisateurFormatTableau['prenom']);
    }
    public static function recupererUtilisateurs() : array {
        require_once "ConnexionBaseDeDonnees.php";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM utilisateur");
        $utilisateurs = array();
        foreach($pdoStatement as $utilisateurFormatTableau){
            $utilisateurs[] = ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $utilisateurs;
    }


    public static function recupererUtilisateurParLogin(string $login) : ?ModeleUtilisateur {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();
        if ($utilisateurFormatTableau === false) {
            afficherErreur("Utilisateur non trouvé");
        }
        return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter(): void {
        $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:loginTag, :nomTag, :prenomTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginTag" => $this->login,
            "nomTag" => $this->nom,
            "prenomTag" => $this->prenom
        );
        $pdoStatement->execute($values);
    }


}
?>
