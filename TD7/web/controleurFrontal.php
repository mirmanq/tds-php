<?php
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

use App\Covoiturage\Controleur\ControleurUtilisateur;

// initialisation en activant l'affichage de débogage
//$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(true);
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');


$controleur = 'Generique';
if (isset($_GET['controleur'])) {
    $controleur = $_GET['controleur'];
}

$nomDeClasseControleur = 'App\Covoiturage\Controleur\Controleur' . ucfirst($controleur);

$action = 'afficherListe';
if (isset($_GET['action'])) {
    $action = $_GET['action'];
}

if (class_exists($nomDeClasseControleur)) {
    $liste_actions = get_class_methods($nomDeClasseControleur); // liste des méthodes de la classe

    if (in_array($action, $liste_actions)) { // on vérifie que l'action demandée est bien une méthode de la classe
        $nomDeClasseControleur::$action(); // appel de l'action

    } else {
        ControleurUtilisateur::afficherErreur("Action inconnue");

    }
} else {
    ControleurUtilisateur::afficherErreur("Contrôleur inconnu");
}
?>