<?php

namespace App\Covoiturage\Configuration;

class ConfigurationSite
{

    public static function getDureeExpirationSession(): int
    {
        return 60 * 60 * 24;
    }

}