<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use PDO;

abstract class AbstractRepository
{
    public function mettreAJour(AbstractDataObject $objet): void
    {
        $nomTable = $this->getNomTable();
        $nomClePrimaire = $this->getNomClePrimaire();
        $nomsColonnes = $this->getNomsColonnes();

        $setClause = join(", ", array_map(function ($nomColonne) {
            return "$nomColonne = :$nomColonne" . "Tag";
        }, $nomsColonnes));

        $sql = "UPDATE $nomTable SET $setClause WHERE $nomClePrimaire = :$nomClePrimaire" . "Tag";
        echo $sql; // Affiche la requête générée pour vérification

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = $this->formatTableauSQL($objet);
        $boundValues = [];
        foreach ($values as $key => $value) {
            $boundValues[$key . "Tag"] = $value;
        }
        $boundValues[$nomClePrimaire . "Tag"] = $values[$nomClePrimaire];

        $pdoStatement->execute($boundValues);
    }

    public function ajouter(AbstractDataObject $objet): bool
    {
        $nomsColonnes = join(", ", $this->getNomsColonnes());
        $valeursColonnes = join(", ", array_map(function ($nomColonne) {
            return ":" . $nomColonne . "Tag";
        }, $this->getNomsColonnes()));

        $sql = "INSERT INTO " . $this->getNomTable() . " ($nomsColonnes) VALUES ($valeursColonnes);";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = $this->formatTableauSQL($objet);
        $boundValues = [];
        foreach ($values as $key => $value) {
            $boundValues[$key . "Tag"] = $value;
        }

        $pdoStatement->execute($boundValues);

        return true;
    }

    public function recuperer(): array
    {
        $nomTable = $this->getNomTable();
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM $nomTable;");
        $tableau = [];

        foreach ($pdoStatement as $ligne) {
            $tableau[] = $this->construireDepuisTableauSQL($ligne);
        }

        return $tableau;
    }


    public function recupererParClePrimaire(string $clePrimaire): ?AbstractDataObject
    {
        $sql = "SELECT * from " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = :clePrimaireTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "clePrimaireTag" => $clePrimaire,
        );
        $pdoStatement->execute($values);

        $objetFormatTableau = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        if ($objetFormatTableau === false) {
            return null;
        }
        return $this->construireDepuisTableauSQL($objetFormatTableau);
    }

    public function supprimer($valeurClePrimaire): void
    {
        $sql = "DELETE FROM " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = :valeurClePrimaire";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute([
            "valeurClePrimaire" => $valeurClePrimaire
        ]);
    }

    //Toutes les classes filles doivent implémenter ces méthodes abstraites
    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;

    /** @return string[] */
    protected abstract function getNomsColonnes(): array;

    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau): AbstractDataObject;

    protected abstract function getNomTable(): string;

    protected abstract function getNomClePrimaire(): string;
}