<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use \DateTime as DateTime;

class TrajetRepository extends AbstractRepository
{
    static public function recupererPassagers(Trajet $trajet): array
    {
        $pdo = ConnexionBaseDeDonnees::getPDO();
        $pdoStatement = $pdo->prepare("SELECT * FROM passager p JOIN utilisateur u ON p.passagerLogin = u.login WHERE p.trajetId = :trajetId;");
        $pdoStatement->execute([
            "trajetId" => $trajet->getId()
        ]);

        $passagers = [];
        foreach ($pdoStatement as $passagerFormatTableau) {
            $passagers[] = (new UtilisateurRepository())->construireDepuisTableauSQL($passagerFormatTableau);
        }

        return $passagers;
    }

    public static function recupererTrajetParId(int $id): Trajet
    {
        $pdo = ConnexionBaseDeDonnees::getPDO();
        $pdoStatement = $pdo->prepare("SELECT * FROM trajet WHERE id = :id;");
        $pdoStatement->execute([
            "id" => $id
        ]);

        $trajetFormatTableau = $pdoStatement->fetch();

        return (new TrajetRepository())->construireDepuisTableauSQL($trajetFormatTableau);
    }

    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $objet): array
    {
        /** @var Trajet $objet */
        return [
            'id' => $objet->getId(),
            'depart' => $objet->getDepart(),
            'arrivee' => $objet->getArrivee(),
            'date' => $objet->getDate()->format('Y-m-d H:i:s'),
            'prix' => $objet->getPrix(),
            'conducteurLogin' => $objet->getConducteurLogin()->getLogin(),
            'nonFumeur' => $objet->isNonFumeur()
        ];
    }



    protected function construireDepuisTableauSQL(array $trajetTableau): Trajet
    {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau["conducteurLogin"]),
            $trajetTableau["nonFumeur"],
            []
        );

        TrajetRepository::recupererPassagers($trajet);

        return $trajet;
    }

    protected function getNomTable(): string
    {
        return "trajet";
    }

    protected function getNomClePrimaire(): string
    {
        return "id";
    }
}