<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use PDO;
use App\Covoiturage\Modele\DataObject\Trajet;

class UtilisateurRepository extends AbstractRepository
{

    public static function recupererTrajetsCommePassager(Utilisateur $utilisateur): array
    {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT t.* FROM trajet t
                INNER JOIN passager p ON t.id = p.trajetId
                WHERE p.passagerLogin = :login";

        $pdoStatement = $pdo->prepare($sql);
        $pdoStatement->execute(['login' => $utilisateur->getLogin()]);

        $trajets = [];
        while ($trajetData = $pdoStatement->fetch(PDO::FETCH_ASSOC)) {
            $trajets[] = (new TrajetRepository)->construireDepuisTableauSQL($trajetData);
        }

        return $trajets;
    }

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Utilisateur $utilisateur */
        return [
            "login" => $utilisateur->getLogin(),
            "nom" => $utilisateur->getNom(),
            "prenom" => $utilisateur->getPrenom(),
        ];
    }

    protected function construireDepuisTableauSQL(array $objetFormatTableau): Utilisateur
    {
        return new Utilisateur(
            $objetFormatTableau["login"],
            $objetFormatTableau["nom"],
            $objetFormatTableau["prenom"]
        );
    }

    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["login", "nom", "prenom"];
    }

    protected function getNomTable(): string
    {
        return "utilisateur";
    }

    protected function getNomClePrimaire(): string
    {
        return "login";
    }
}