<?php

namespace App\Covoiturage\Modele\HTTP;

use App\Covoiturage\Configuration\ConfigurationSite;
use Exception;

class Session
{
    private static ?Session $instance = null;

    /**
     * @throws Exception
     */
    private function __construct()
    {
        if (session_start() === false) {
            throw new Exception("La session n'a pas réussi à démarrer.");
        }
    }

    public static function getInstance(): Session
    {
        if (is_null(Session::$instance)) {
            Session::$instance = new Session();
            Session::$instance->verifierDerniereActivite();
        }
        return Session::$instance;
    }

    private function verifierDerniereActivite(): void
    {
        $dureeExpiration = ConfigurationSite::getDureeExpirationSession();
        if (isset($_SESSION['derniere_activite']) && (time() - $_SESSION['derniere_activite']) > $dureeExpiration) {
            $this->detruire();
        }
        $_SESSION['derniere_activite'] = time();
    }

    public function contient($nom): bool
    {
        return isset($_SESSION[$nom]);
    }

    public function enregistrer(string $nom, mixed $valeur): void
    {
        $_SESSION[$nom] = serialize($valeur);
    }

    public function lire(string $nom): mixed
    {
        return isset($_SESSION[$nom]) ? unserialize($_SESSION[$nom]) : null;
    }

    public function supprimer($nom): void
    {
        unset($_SESSION[$nom]);
    }

    public function detruire(): void
    {
        session_unset();
        session_destroy();
        Cookie::supprimer(session_name());
        Session::$instance = null;
    }
}
?>