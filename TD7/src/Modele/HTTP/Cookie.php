<?php

namespace App\Covoiturage\Modele\HTTP;

use App\Covoiturage\Controleur\ControleurUtilisateur;

class Cookie
{

    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void
    {
        $dureeExpiration = $dureeExpiration ?? time() + 3600;
        setcookie($cle, $valeur, $dureeExpiration);
    }

   public static function lire(string $cle): mixed
   {
        return $_COOKIE[$cle] ?? null;
   }

    public static function contient($cle) : bool
    {
        return array_key_exists("nomCookie", $_COOKIE);
    }

    public static function supprimer($cle) : void
    {
        unset($_COOKIE[$cle]);
        setcookie ($cle, "", 1);

    }
}