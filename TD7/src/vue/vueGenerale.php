<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/tds-php/TD7/ressources/style.css">

    <title><?php
        /**
         * @var string $titre
         */
        echo $titre; ?>
    </title>
</head>
<body>
<header>
    <nav>
        <nav>
            <ul>
                <li>
                    <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des
                        utilisateurs</a>
                </li>
                <li>
                    <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des
                        trajets</a>
                </li>
                <li>
                    <a href="controleurFrontal.php?action=afficherFormulairePreference"><img src="/tds-php/TD7/ressources/img/img.png"> </a></li>
                </li>
            </ul>
        </nav>
    </nav>
</header>
<main>
    <?php
    /**
     * @var string $cheminCorpsVue
     */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de Quentin Company - 2020
    </p>
</footer>
</body>
</html>