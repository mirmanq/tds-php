<?php
use App\Covoiturage\Lib\PreferenceControleur;

// Lire la préférence actuelle
$preference = PreferenceControleur::existe() ? PreferenceControleur::lire() : '';
?>

<form method="get" action="controleurFrontal.php">
    <input type="hidden" name="action" value="enregistrerPreference" />
    <fieldset>
        <legend>Formulaire de choix de page préferé :</legend>

        <p class="InputAddOn">
            <label for="utilisateurId">Utilisateur</label>
            <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur" <?php echo $preference === 'utilisateur' ? 'checked' : ''; ?>>
        </p>

        <p class="InputAddOn">
            <label for="trajetId">Trajet</label>
            <input type="radio" id="trajetId" name="controleur_defaut" value="trajet" <?php echo $preference === 'trajet' ? 'checked' : ''; ?>>
        </p>

        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>