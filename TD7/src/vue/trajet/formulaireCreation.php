<form method="get" action="controleurFrontal.php">
    <input type="hidden" name="action" value="creerDepuisFormulaire" />
    <input type="hidden" name="controleur" value="trajet" />
    <fieldset>
        <legend>Formulaire de création trajet :</legend>
        <p class="InputAddOn">
            <input type="hidden" name="controleur" value="trajet">
        </p>

        <p class="InputAddOn">
            <label for="depart">Départ :</label>
            <input type="text" id="depart" name="depart" required>
        </p>

        <p class="InputAddOn">
            <label for="arrivee">Arrivée :</label>
            <input type="text" id="arrivee" name="arrivee" required>
        </p>

        <p class="InputAddOn">
            <label for="date">Date :</label>
            <input type="date" id="date" name="date" required>
        </p>

        <p class="InputAddOn">
            <label for="prix">Prix :</label>
            <input type="number" id="prix" name="prix" required>
        </p>

        <p class="InputAddOn">
            <label for="conducteurLogin">Conducteur :</label>
            <input type="text" id="conducteurLogin" name="conducteurLogin" required>
        </p>

        <p class="InputAddOn">
            <input type="hidden" name="nonFumeur" value="0">
            <label for="nonFumeur">Non Fumeur :</label>
            <input type="checkbox" id="nonFumeur" name="nonFumeur" value="1">
        </p>

        <p>
            <input type="submit" value="Envoyer"/>
        </p>
    </fieldset>

</form>