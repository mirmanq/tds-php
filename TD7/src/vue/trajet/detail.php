<?php
/** @var \App\Covoiturage\Modele\DataObject\Trajet $trajet */
$id = htmlspecialchars($trajet->getId());
$depart = htmlspecialchars($trajet->getDepart());
$arrivee = htmlspecialchars($trajet->getArrivee());
$date = htmlspecialchars($trajet->getDate()->format('d/m/Y H:i'));
$passagers = htmlspecialchars(implode(', ', $trajet->getPassagers()));
?>

<p>Id : <?php echo $id; ?> - Départ : <?php echo $depart; ?> - Arrivée : <?php echo $arrivee; ?> - Date : <?php echo $date; ?> - Passagers : <?php echo $passagers; ?></p>