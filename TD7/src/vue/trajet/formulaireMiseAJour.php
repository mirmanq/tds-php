çà<?php
/**
 * @var Trajet $trajet
 */
?>

<form method="get" action="controleurFrontal.php">
    <input type="hidden" name="action" value="mettreAJour" />
    <input type="hidden" name="controleur" value="trajet" />
    <fieldset>
        <legend>Formulaire de modification d'un trajet :</legend>
        <p class="InputAddOn">
            <input type="hidden" name="controleur" value="trajet">
        </p>

        <p class="InputAddOn">
            <input type="hidden" id="id" name="id" value="<?= htmlspecialchars($trajet->getId()); ?>" readonly="readonly">
        </p>

        <p class="InputAddOn">
            <label for="depart">Départ :</label>
            <input class="InputAddOn-field" type="text" id="depart" name="depart" value="<?= htmlspecialchars($trajet->getDepart()); ?>" required>
        </p>

        <p class="InputAddOn">
            <label for="arrivee">Arrivée :</label>
            <input class="InputAddOn-field" type="text" id="arrivee" name="arrivee" value="<?= htmlspecialchars($trajet->getArrivee()); ?>" required>
        </p>

        <p class="InputAddOn">
            <label for="date">Date :</label>
            <input class="InputAddOn-field" type="date" id="date" name="date" value="<?= htmlspecialchars($trajet->getDate()->format('Y-m-d')); ?>" required>
        </p>

        <p class="InputAddOn">
            <label for="prix">Prix :</label>
            <input class="InputAddOn-field" type="number" id="prix" name="prix" value="<?= htmlspecialchars($trajet->getPrix()); ?>" required>
        </p>

        <p class="InputAddOn">
            <label for="conducteurLogin">Conducteur :</label>
            <input class="InputAddOn-field" type="text" id="conducteurLogin" name="conducteurLogin" value="<?= htmlspecialchars($trajet->getConducteurLogin()->getLogin()); ?>" required>
        </p>

        <p class="InputAddOn">
            <label for="nonFumeur">Non Fumeur :</label>
            <input class="InputAddOn-field" type="checkbox" id="nonFumeur" name="nonFumeur" <?= $trajet->isNonFumeur() ? 'checked' : '' ?>>
        </p>

        <p>
            <input type="submit" value="Envoyer"/>
        </p>
    </fieldset>
</form>