<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use http\Cookie;

class ControleurUtilisateur extends ControleurGenerique
{

    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer();

        $parametres = [
            'titre' => 'Liste des utilisateurs',
            'cheminCorpsVue' => 'utilisateur/liste.php',
            'utilisateurs' => $utilisateurs
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function afficherDetail(): void
    {
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if ($utilisateur === null) {
            self::afficherErreur("Utilisateur non trouvé");
            return;
        }

        $parametres = [
            'titre' => 'Détail de l\'utilisateur',
            'cheminCorpsVue' => 'utilisateur/detail.php',
            'utilisateur' => $utilisateur
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function demmarerSession(): void
    {
        session_start();
        echo "Session démarrée.";
    }
    public static function afficherFormulaireCreation(): void
    {
        $parametres = [
            'titre' => 'Créer un utilisateur',
            'cheminCorpsVue' => 'utilisateur/formulaireCreation.php'
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function creerDepuisFormulaire(): void
    {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->ajouter($utilisateur);

        $utilisateurs = (new UtilisateurRepository())->recuperer();

        $parametres = [
            'titre' => 'Utilisateur créé',
            'cheminCorpsVue' => 'utilisateur/utilisateurCree.php',
            'utilisateurs' => $utilisateurs
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $login = $tableauDonneesFormulaire['login'];
        $nom = $tableauDonneesFormulaire['nom'];
        $prenom = $tableauDonneesFormulaire['prenom'];
        // Ajoutez d'autres champs nécessaires ici

        return new Utilisateur($login, $nom, $prenom);
    }

//    public static function deposerCookie(): void
//    {
//        $nomCookie = 'monCookie';
//        $valeurCookie = 'valeurDuCookie';
//        $duree = time() + 3600; // 1 heure
//
//        Cookie::enregistrer($nomCookie, $valeurCookie, $duree);
//
//        echo "Cookie déposé avec succès.";
//    }
//
//    public static function lireCookie(): void
//    {
//        $nomCookie = 'monCookie';
//
//        if (isset($_COOKIE[$nomCookie])) {
//            echo "Valeur du cookie '{$nomCookie}' : {Cookie::lire($nomCookie)}";
//        } else {
//            echo "Cookie '{$nomCookie}' non trouvé.";
//        }
//    }

    public static function afficherFormulaireMiseAJour(): void
    {
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if ($utilisateur === null) {
            self::afficherErreur("Utilisateur non trouvé");
            return;
        }

        $parametres = [
            'titre' => 'Mettre à jour un utilisateur',
            'cheminCorpsVue' => 'utilisateur/formulaireMiseAJour.php',
            'utilisateur' => $utilisateur
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function mettreAJour(): void
    {
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];

        if (empty($login) || empty($nom) || empty($prenom)) {
            self::afficherErreur("Tous les champs sont obligatoires");
            return;
        }

        $utilisateur = new Utilisateur($login, $nom, $prenom);
        (new UtilisateurRepository())->mettreAJour($utilisateur);

        $utilisateurs = (new UtilisateurRepository())->recuperer();

        $parametres = [
            'titre' => 'Utilisateur mis à jour',
            'cheminCorpsVue' => 'utilisateur/utilisateurMiseAJour.php',
            'utilisateurs' => $utilisateurs
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function supprimer(): void
    {
        $login = $_GET['login'];

        if (empty($login)) {
            self::afficherErreur("Le login n'a pas été trouvé");
            return;
        }

        (new UtilisateurRepository())->supprimer($login);

        $utilisateurs = (new UtilisateurRepository())->recuperer();

        $parametres = [
            'titre' => 'Utilisateur supprimé',
            'cheminCorpsVue' => 'utilisateur/utilisateurSupprime.php',
            'utilisateurs' => $utilisateurs
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function afficherErreur(string $messageErreur = ""): void
    {
        $parametres = [
            'titre' => 'Erreur',
            'cheminCorpsVue' => 'utilisateur/erreur.php',
            'messageErreur' => $messageErreur
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

}

?>