<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{
    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres);

        require __DIR__ . '/../vue/' . $cheminVue;
    }

    public static function afficherFormulairePreference()
    {
        $parametres = [
            'titre' => 'Préférences',
            'cheminCorpsVue' => 'formulairePreference.php'
        ];

        self::afficherVue('vueGenerale.php', $parametres);

    }

    public static function enregistrerPreference(): void
    {
        $preference = $_GET['controleur_defaut'] ?? '';

        if (!empty($preference)) {
            PreferenceControleur::enregistrer($preference);
        }

        $parametres = [
            'titre' => 'Préférence enregistrée',
            'cheminCorpsVue' => 'preferenceEnregistree.php'
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

}