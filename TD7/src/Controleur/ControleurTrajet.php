<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurTrajet extends ControleurGenerique
{
    public static function afficherListe(): void
    {
        $trajets = (new TrajetRepository())->recuperer();

        $parametres = [
            'titre' => 'Liste des trajets',
            'cheminCorpsVue' => 'trajet/liste.php',
            'trajets' => $trajets
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function afficherErreur(string $messageErreur = ""): void
    {
        $parametres = [
            'titre' => 'Erreur',
            'cheminCorpsVue' => 'trajet/erreur.php',
            'messageErreur' => $messageErreur
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }


    public static function afficherDetail(): void
    {
        $id = $_GET['id'];

        if (empty($id)) {
            self::afficherErreur("L'id n'a pas été trouvé");
            return;
        }

        $trajet = TrajetRepository::recupererTrajetParId($id);

        $parametres = [
            'titre' => 'Détail du trajet',
            'cheminCorpsVue' => 'trajet/detail.php',
            'trajet' => $trajet
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function supprimer(): void
    {
        $id = $_GET['id'];

        if (empty($id)) {
            self::afficherErreur("L'id du trajet n'a pas été trouvé");
            return;
        }

        (new TrajetRepository())->supprimer($id);

        $trajets = (new TrajetRepository())->recuperer(); // récupérer la liste des trajets pour l'afficher après la suppression

        $parametres = [
            'titre' => 'Trajet supprimé',
            'cheminCorpsVue' => 'trajet/trajetSupprime.php',
            'trajets' => $trajets
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function afficherFormulaireCreation(): void
    {
        $parametres = [
            'titre' => 'Créer un nouveau trajet',
            'cheminCorpsVue' => 'trajet/formulaireCreation.php'
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }


    public static function creerDepuisFormulaire(): void
    {
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->ajouter($trajet);

        $trajets = (new TrajetRepository())->recuperer();

        $parametres = [
            'titre' => 'Trajet créé',
            'cheminCorpsVue' => 'trajet/trajetCree.php',
            'trajets' => $trajets
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function afficherFormulaireMiseAJour(): void
    {
        $id = $_GET['id'] ?? null;

        if ($id === null) {
            echo "Erreur : ID de trajet manquant.";
            return;
        }

        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        if ($trajet === null) {
            echo "Erreur : Trajet non trouvé.";
            return;
        }

        $parametres = [
            'titre' => 'Modifier un trajet',
            'cheminCorpsVue' => 'trajet/formulaireMiseAJour.php',
            'trajet' => $trajet
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function mettreAJour(): void
    {
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->mettreAJour($trajet);

        $trajets = (new TrajetRepository())->recuperer();

        $parametres = [
            'titre' => 'Trajet mis à jour',
            'cheminCorpsVue' => 'trajet/trajetMiseAJour.php',
            'trajets' => $trajets
        ];

        self::afficherVue('vueGenerale.php', $parametres);
    }

    /**
     * @return Trajet
     * @throws \DateMalformedStringException
     */
    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $id = $tableauDonneesFormulaire['id'] ?? null;
        $depart = $tableauDonneesFormulaire['depart'];
        $arrivee = $tableauDonneesFormulaire['arrivee'];
        $date = new \DateTime($tableauDonneesFormulaire['date']);
        $prix = $tableauDonneesFormulaire['prix'];
        $conducteurLogin = $tableauDonneesFormulaire['conducteurLogin'];
        $nonFumeur = isset($tableauDonneesFormulaire['nonFumeur']) ? 1 : 0;

        $conducteur = (new UtilisateurRepository())->recupererParClePrimaire($conducteurLogin);

        return new Trajet($id, $depart, $arrivee, $date, $prix, $conducteur, $nonFumeur, []);
    }
}