<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
    <H1>liste des utilisateurs</H1>
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
//          $texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
//          echo $texte;

//        $utilisateur = [
//            'prenom' => 'Marc',
//            'nom' => 'Delacour',
//            'login' => 'MarcDelacour'
//        ];
//
//        $utilisateur2 = [
//            'prenom' => 'Esteban',
//            'nom' => 'Remond',
//            'login' => 'EstebanR'
//        ];
//
//        $utilisateur3 = [
//            'prenom' => 'Quentin',
//            'nom' => 'Mirman',
//            'login' => 'QuentinM'
//        ];
//
//        $utilisateurs = [
//            "1" => $utilisateur,
//            "2" => $utilisateur2,
//            "3" => $utilisateur3
//        ];
//
////       var_dump($utilisateurs);
//
////        echo "Utilisateur ". $utilisateur['prenom'] . " " . $utilisateur['nom'] . " de login " . $utilisateur['login'];
//        if (empty($utilisateurs)) {
//            echo "Il n’y a aucun utilisateur.";}
//        else { foreach ($utilisateurs as $utilisateur) {
//            echo "<ul> <li    > Utilisateur  $utilisateur[prenom] $utilisateur[nom]  de login  $utilisateur[login] </li> </ul>";
//            }
        $prenom = "Marc";

        echo "Bonjour\n " . $prenom;
        echo "Bonjour\n $prenom";
        echo 'Bonjour\n $prenom';

        echo $prenom;
        echo "$prenom";
        
        ?>
    </body>
</html> 